﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Casper;
using Casper.Graphic;
using Casper.Inputs;

namespace Snake {
	class Snake : Game {
		byte red = 0, green = 0, blue = 0;

		public Snake ( ) : base ( 500, 500, "Hello World!" ) {
			ErrorCallbackEvent += ( object sender, ErrorEventArgs e ) => {
				Console.WriteLine ( e.description );
			};
			Keyboard.KeyboardCallbackEvent += UpdateColor;
			Keyboard.KeyboardCallbackEvent += MousePostion;

			Mouse.SetMode ( CursorMode.NORMAL );

			Window.WindowResizeCallbackEvent += WindowResize;
			Window.WindowPosChangeCallbackEvent += WindowPosition;
			Window.WindowMaximizeCallbackEvent += WindowMaximize;
		}

		private void WindowMaximize ( object sender, WindowMaximizeArgs args ) {
			Console.WriteLine ( $"Window {( args.Maximize ? "Maximize" : "Restore" )} | MAXIMIZE: {Window.IsMaximized}" );
		}

		private void WindowPosition ( object sender, WindowPosArgs args ) {
			Console.WriteLine ( $"XPos: {args.XPosition} | YPosition: {args.YPosition}" );
		}

		private void WindowResize ( object sender, WindowResizeArgs args ) {
			Console.WriteLine ( $"Width: {args.Width} | Height: {args.Height}" );
		}

		private void MousePostion ( object sender, KeyboardEventArgs args ) {
			Vector2 position = Mouse.GetMousePsition ( );
			int amount = args.modifier == KeyMods.SHIFT ? 7 : 1;

			position = args.key switch
			{
				KeyCode.SPACE => new Vector2 ( 0, 0 ),
				KeyCode.ENTER => new Vector2 ( Window.Width, Window.Height ),
				KeyCode.J => new Vector2 ( 0, amount ) + position,
				KeyCode.K => new Vector2 ( 0, -amount ) + position,
				KeyCode.L => new Vector2 ( amount, 0 ) + position,
				KeyCode.H => new Vector2 ( -amount, 0 ) + position,
				_ => position
			};
			Mouse.SetMousePosition ( position );
			Console.WriteLine ( $"X: {position.X} | Y: {position.Y}" );
		}

		public void UpdateColor ( object sender, KeyboardEventArgs args ) {
			byte amount = ( byte ) ( args.modifier == KeyMods.SHIFT ? -7 : 7 );
			if ( args.key == KeyCode.LEFT ) {
				blue += amount;
			} else if ( args.key == KeyCode.DOWN ) {
				green += amount;
			} else if ( args.key == KeyCode.RIGHT ) {
				red += amount;
			}

			if ( args.key == KeyCode.ESCAPE ) {
				if ( Window.IsMaximized ) {
					Window.RestoreWindow ( );
				} else {
					Window.MaximizeWindow ( );
				}
			}
		}

		public override void OnLoad ( ) {
			base.OnLoad ( );
		}

		public override void Update ( ) {
			base.Update ( );

		}

		public override void Draw ( ) {
			base.Draw ( );
			SetClearColor ( red, green, blue, 1f );
		}
	}
}
