#!/bin/bash

set -e

CURDIR=${PWD}
OPENGLDIR="../OpenGL"

cd "$OPENGLDIR"
git submodule foreach git pull origin master
echo "Compiling GLFW's OpenGL"
cmake . > /dev/null && make > /dev/null

VERSION=$(cat README.md | awk -F "version " '/stable release is/{print $2}')
VERSION=${VERSION::-1}

echo "Current version of GLFW is ${VERSION}"

cd "$CURDIR"

if [[ -d lib ]] && [[ -f lib/version ]]; then
	CURVER=$(cat lib/version)
	if [[ "$VERSION" == "$CURVER" ]]; then
		echo "Everything's good"
		exit 0
	fi
fi

wget --show-progress --quiet\
	"https://github.com/glfw/glfw/releases/download/${VERSION}/glfw-${VERSION}.bin.WIN64.zip"\
	-O GLFW.zip

if [[ -d lib2 ]]; then
	echo "Removing oldest library backup"
	rm -r lib2
fi
if [[ -d lib1 ]]; then
	echo "Saving v3"
	mv lib1 lib2
fi
if [[ -d lib ]]; then
	echo "Saving v2"
	mv lib lib1
fi

echo "Copying over newest DLL"
mkdir lib
unzip GLFW.zip > /dev/null
cp glfw-*.bin*/lib-vc2015/* lib/
cp glfw-*.bin*/include/GLFW/* lib/

echo $VERSION > lib/version

echo "Cleaning up..."
rm -r GLFW.zip
rm -r glfw-*.bin*

echo "Done!"
