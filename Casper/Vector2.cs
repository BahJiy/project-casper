﻿using System;
namespace Casper {
	/// <summary>
	/// A class contain a 2D vector
	/// </summary>
	public sealed class Vector2 {
		public double X { get; set; } = 0;
		public double Y { get; set; } = 0;

		public Vector2 ( double x, double y ) {
			this.X = x;
			this.Y = y;
		}

		/// <summary>
		/// Return the direction of the vector in radians
		/// </summary>
		/// <returns>The direction of the vector from PI to -PI in radian</returns>
		public double Direction ( ) => Math.Atan ( Y / X );
		/// <summary>
		/// Return the direction of the vector in degrees
		/// </summary>
		/// <returns>The direction of the vector from 0 - 180 in degree</returns>
		public double DirectionDegrees ( ) => Direction ( ) * 180 / Math.PI;
		/// <summary>
		/// Returns the magnitude of the vector sqrt(X^2 + Y^2)
		/// </summary>
		/// <returns>The magnitude of the vector</returns>
		public double Magnitude ( ) => Math.Sqrt ( Math.Pow ( X, 2 ) + Math.Pow ( Y, 2 ) );
		/// <summary>
		/// Return if this vector is perpendicular with the  vector. Note that
		/// due to rounding, this may not be exact
		/// </summary>
		/// <param name="vector">The second vector</param>
		/// <returns>True if the vector is perpendicular to this vector; else false</returns>
		public bool IsPerpendicular ( Vector2 vector ) => Math.Round ( Math.Acos ( this * vector ), 10 ) == Math.Round ( Math.PI / 2, 10 );
		/// <summary>
		/// Return if this vector is parallel with the second  vector. Note that
		/// due to rounding, this may not be exact
		/// </summary>
		/// <param name="vector">The second vector</param>
		/// <returns>True if the vector is parallel to this vector; else false</returns>
		public bool IsParallel ( Vector2 vector ) {
			double angle = Math.Round ( Math.Acos ( this * vector ), 10 );
			return angle == Math.Round ( Math.PI, 10 ) || angle == 0;
		}

		public static Vector2 operator + ( Vector2 v1, Vector2 v2 ) => new Vector2 ( v1.X + v2.X, v1.Y + v2.Y );
		public static Vector2 operator - ( Vector2 v1, Vector2 v2 ) => new Vector2 ( v1.X - v2.X, v1.Y - v2.Y );
		public static double operator * ( Vector2 v1, Vector2 v2 ) => v1.X * v2.X + v1.X + v2.Y;

		public override string ToString ( ) => $"X: {X} Y: {Y}";
		public override bool Equals ( object obj ) => obj is Vector2 vector ? X == vector.X && Y == vector.Y : false;
		public override int GetHashCode ( ) => ( int ) ( X * 13 + Y * 31 );
	}
}
