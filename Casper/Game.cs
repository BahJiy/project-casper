﻿using System;
using System.IO;
using Casper.DLLImport;
using Casper.Graphic;
using Casper.Inputs;

namespace Casper {
	public partial class Game : IDisposable {
		public readonly Window Window;
		/// <summary>
		/// Handles all keyboard activities
		/// </summary>
		public readonly Keyboard Keyboard;
		/// <summary>
		/// Handles all mouse activities
		/// </summary>
		public readonly Mouse Mouse;

		/// <summary>
		/// Create a Game with a set width, height and title
		/// </summary>
		/// <param name="width">The width of the game</param>
		/// <param name="height">The height of the game</param>
		/// <param name="title">The title of the game</param>
		public Game ( int width = 640, int height = 800, string title = "OpenGL Game" ) {
			GLFW.SetErrorCallback ( ErrorCallbackHander );
			ErrorCallbackEvent += ( sender, args ) => {
				using ( StreamWriter writer = new StreamWriter ( @".\error.log", true ) ) {
					writer.WriteLine ( $"{DateTime.Now.ToShortDateString ( )} {DateTime.Now.ToLongTimeString ( )} - Error Code: {args.errorCode} = {args.description}" );
				}
			};
			Window = new Window ( title );
			Window.CreateWindow ( width, height );
			Window.Position = new Vector2 ( 50, 50 );

			Keyboard = new Keyboard ( Window );
			Mouse = new Mouse ( Window );
		}

		/// <summary>
		/// Execute the game
		/// </summary>
		public void Run ( ) {
			OnLoad ( );
			do {
				Update ( );
				Draw ( );
				GLFW.GLFWPollEvents ( );
			} while ( !Window.IsExiting ( ) );
			OnUnload ( );
		}

		/// <summary>
		/// Activities to do before the game loads
		/// </summary>
		public virtual void OnLoad ( ) {

		}

		/// <summary>
		/// Activities to do after the once the game exits
		/// </summary>
		public virtual void OnUnload ( ) {
			Dispose ( );
		}

		/// <summary>
		/// The main update logic for the game
		/// </summary>
		public virtual void Update ( ) {

		}

		/// <summary>
		/// The main draw logic for the game
		/// </summary>
		public virtual void Draw ( ) {
			Window.Update ( );
		}

		/// <summary>
		/// Set the clear color
		/// </summary>
		/// <param name="red">Red 0 - 255</param>
		/// <param name="green">Green 0 - 255</param>
		/// <param name="blue">Blue 0 - 255</param>
		/// <param name="alpha">Alpha 0 - 255</param>
		public void SetClearColor ( byte red, byte green, byte blue, float alpha = 1.0f )
			=> OpenGL.ClearColor ( red / 255f, green / 255f, blue / 255f, alpha );

		/// <summary>
		/// Get the current version of GLFW loaded
		/// </summary>
		/// <returns>A String formated of the GLFW Version</returns>
		public static string GetGLFWVersion ( ) {
			GLFW.GetVersion ( out int major, out int minor, out int rev );
			return String.Format ( "GLFW Version: {0}.{1} rev {2}", major, minor, rev );
		}

		/// <summary>
		/// Get the current version of OpenGL used
		/// </summary>
		/// <returns></returns>
		public static string GetOpenGLVersion ( )
			=> OpenGL.GetString ( OpenGL.Properties.Version );

		/// <summary>
		/// Clean up Game resources
		/// </summary>
		public void Dispose ( ) {
			Window.Dispose ( );
		}
	}
}
