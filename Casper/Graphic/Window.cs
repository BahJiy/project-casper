﻿using System;
using Casper.DLLImport;

namespace Casper.Graphic {
	public sealed partial class Window : IDisposable {
		public int Width {
			get { return ( int ) Size.X; }
			set { Size = new Vector2 ( value, Size.Y ); }
		}
		public int Height {
			get { return ( int ) Size.Y; }
			set { Size = new Vector2 ( Size.X, value ); }
		}
		public Vector2 Size {
			get {
				GLFW.GetWindowSize ( window, out int x, out int y );
				return new Vector2 ( x, y );
			}
			set {
				GLFW.SetWindowSize ( window, ( int ) value.X, ( int ) value.Y );
			}
		}

		public int X {
			get { return ( int ) Position.X; }
			set { Position = new Vector2 ( value, Position.Y ); }
		}
		public int Y {
			get { return ( int ) Position.Y; }
			set { Position = new Vector2 ( Position.X, value ); }
		}
		public Vector2 Position {
			get {
				GLFW.GetWindowPosition ( window, out int x, out int y );
				return new Vector2 ( x, y );
			}
			set {
				GLFW.SetWindowPosition ( window, ( int ) value.X, ( int ) value.Y );
			}
		}

		public bool IsMaximized {
			get => GLFW.GetWindowAttributes ( window, ( int ) GLFWEnum.MAXIMIZED ) == 1;
		}
		public bool IsVisible {
			get => GLFW.GetWindowAttributes ( window, ( int ) GLFWEnum.VISIBLE ) == 1;
		}
		public bool IsFocused {
			get => GLFW.GetWindowAttributes ( window, ( int ) GLFWEnum.FOCUSED ) == 1;
		}

		public string Title { get; private set; }

		internal IntPtr window { get; private set; }

		public static implicit operator IntPtr ( Window window ) => window.window;

		/// <summary>
		/// Initialize the Window
		/// </summary>
		/// <param name="width">The width of the window</param>
		/// <param name="height">The height of the window</param>
		/// <param name="title">The title of the window</param>
		internal Window ( string title ) {
			Title = title;

			if ( !GLFW.Init ( ) ) {
				Console.WriteLine ( "Initialization Failed! Check ErrorCallback." );
				Environment.Exit ( 1 );
			}
		}

		/// <summary>
		/// Create and set the OpenGL context. Must be called before any OpenGL activites
		/// </summary>
		internal void CreateWindow ( int width, int height ) {
			if ( ( window = GLFW.CreateWindow ( width, height, Title, IntPtr.Zero, IntPtr.Zero ) ) == IntPtr.Zero ) {
				Console.WriteLine ( "Window creation failed! Check ErrorCallback." );
				Environment.Exit ( 1 );
			}
			GLFW.MakeContextCurrent ( window );
			GLFW.SwapInterval ( 1 );

			GLFW.DefaultWindowHints ( );

			GLFW.SetWindowResizeCallback ( window, WindowResizeCallback );
			GLFW.SetWindowPositionCallback ( window, WindowPositionCallback );
			GLFW.SetWindowMaximizeCallback ( window, WindowMaximizeCallback );
		}

		/// <summary>
		/// Check if window is closing
		/// </summary>
		/// <returns>True if window is closing</returns>
		internal bool IsExiting ( ) => GLFW.WindowShouldClose ( window );

		/// <summary>
		/// Close the window. IsExiting will return true
		/// </summary>
		internal void Exit ( ) => GLFW.SetWindowShouldClose ( window, 1 );

		internal void Update ( ) {
			OpenGL.Clear ( OpenGL.BitField.ColorBufferBit );
			OpenGL.Clear ( OpenGL.BitField.DepthBufferBit );
			OpenGL.Clear ( OpenGL.BitField.StencilBufferBit );
			GLFW.SwapBuffers ( window );
		}

		/// <summary>
		/// Clean up resources
		/// </summary>
		public void Dispose ( ) {
			GLFW.DestroyWindow ( window );
			GLFW.Terminate ( );
		}

		/// <summary>
		/// Maximize the window
		/// </summary>
		public void MaximizeWindow ( ) => GLFW.MaximizeWindow ( window );
		/// <summary>
		/// Restore the window
		/// </summary>
		public void RestoreWindow ( ) => GLFW.RestoreWindow ( window );

		/// <summary>
		/// Hide the window
		/// </summary>
		public void HideWindow ( ) => GLFW.HideWindow ( window );

		/// <summary>
		/// Show the window
		/// </summary>
		public void ShowWindow ( ) => GLFW.ShowWindow ( window );

		/// <summary>
		/// Force focus the window
		/// </summary>
		public void FocusWindow ( ) => GLFW.FocusWindow ( window );

		/// <summary>
		/// Request the user attention to this window
		/// </summary>
		public void RequestAttention ( ) => GLFW.RequestWindowAttention ( window );
	}
}
