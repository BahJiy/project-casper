using System;

namespace Casper.Graphic {
	public sealed partial class Window {

		/// <summary>
		/// A method stub for GLFW resize callback
		/// </summary>
		internal delegate void WinResizeCallback ( IntPtr window, int width, int height );
		/// <summary>
		/// Occurs when the window is resized
		/// </summary>
		public event EventHandler<WindowResizeArgs> WindowResizeCallbackEvent;
		private void WindowResizeCallback ( IntPtr window, int width, int height ) =>
			WindowResizeCallbackEvent?.Invoke ( this, new WindowResizeArgs ( ) { Width = width, Height = height } );

		/// <summary>
		/// A method stub for GLFW position callback
		/// </summary>
		internal delegate void WinPosChangeCallback ( IntPtr window, int x, int y );
		/// <summary>
		/// Occurs when the window is resized
		/// </summary>
		public event EventHandler<WindowPosArgs> WindowPosChangeCallbackEvent;
		private void WindowPositionCallback ( IntPtr window, int x, int y ) =>
			WindowPosChangeCallbackEvent?.Invoke ( this, new WindowPosArgs ( ) { XPosition = x, YPosition = y } );

		/// <summary>
		/// A method stub for GLFW maxizmize callback
		/// </summary>
		internal delegate void WinMaximizeCallback ( IntPtr window, int maxizmize );
		/// <summary>
		/// Occurs when the window is maximize or restore
		/// </summary>
		public event EventHandler<WindowMaximizeArgs> WindowMaximizeCallbackEvent;
		private void WindowMaximizeCallback ( IntPtr window, int maximize ) =>
			WindowMaximizeCallbackEvent?.Invoke ( this, new WindowMaximizeArgs ( ) { Maximize = maximize == 1 } );

		/// <summary>
		/// A method stub for GLFW focus callback
		/// </summary>
		internal delegate void WinFocusCallback ( IntPtr window, int focused );
		/// <summary>
		/// Occurs when the window gain focus or lose focus
		/// </summary>
		public event EventHandler<WindowFocusArgs> WindowFocusCallbackEvent;
		private void WindowFocusCallback ( IntPtr window, int focus ) =>
			WindowFocusCallbackEvent?.Invoke ( this, new WindowFocusArgs ( ) { Focused = focus == 1 } );
	}

	/// <summary>
	/// Provides data for the Window.WindowResizeCallbackEvent. This class cannot be inherited.
	/// </summary>
	public sealed class WindowResizeArgs : EventArgs {
		public int Width { get; set; }
		public int Height { get; set; }
	}

	/// <summary>
	/// Provides data for the Window.WindowPosChangeCallbackEvent. This class cannot be inherited.
	/// </summary>
	public sealed class WindowPosArgs : EventArgs {
		public int XPosition { get; set; }
		public int YPosition { get; set; }
	}

	/// <summary>
	/// Provides data for the Window.WindowPosChangeCallbackEvent. This class cannot be inherited.
	/// </summary>
	public sealed class WindowMaximizeArgs : EventArgs {
		public bool Maximize { get; set; }
	}

	/// <summary>
	/// Provides data for the Window.WindowFocusCallbackEvent. This class cannot be inherited.
	/// </summary>
	public sealed class WindowFocusArgs : EventArgs {
		public bool Focused { get; set; }
	}
}
