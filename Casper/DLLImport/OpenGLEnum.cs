﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casper.DLLImport {
	internal static partial class OpenGL {
		internal enum Properties : int {
			MajorVersion = 0x821B,
			MinorVersion = 0x821C,
			Version = 0x1F02,
			Vender = 0x1F00
		}
		internal enum BitField : int {
			DepthBufferBit = 0x00000100,
			StencilBufferBit = 0x00000400,
			ColorBufferBit = 0x00004000
		}
	}
}
