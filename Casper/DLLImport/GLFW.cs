﻿using System;
using System.Runtime.InteropServices;
using Casper.Graphic;

namespace Casper.DLLImport {
	internal static partial class GLFW {
		private const string DLL = "glfw";

		/// <summary>
		/// Get GLFW version information
		/// </summary>
		/// <param name="major">The int pointer for the major version number</param>
		/// <param name="minor">The int pointer for the minor version number</param>
		/// <param name="rev">The int pointer for the revision version number</param>
		[DllImport ( DLL, EntryPoint = "glfwGetVersion" )]
		internal static extern void GetVersion ( out int major, out int minor, out int rev );

		#region Window
		/// <summary>
		/// Get the window status/state
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glfwGetWindowAttrib" )]
		internal static extern int GetWindowAttributes ( IntPtr window, int attribute );

		/// <summary>
		/// Set the window setting
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glfwWindowHint" )]
		internal static extern void SetWindowHints ( int setting, int value );

		/// <summary>
		/// Reset all the window's hinting to the default values
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glfwDefaultWindowHints" )]
		internal static extern void DefaultWindowHints ( );

		/// <summary>
		/// Initialize the GLFW Window
		/// </summary>
		/// <returns>Return true if succeed</returns>
		[DllImport ( DLL, EntryPoint = "glfwInit" )]
		internal static extern bool Init ( );

		/// <summary>
		/// Create a GLFW Window. Initialize must be called first
		/// </summary>
		/// <param name="width">Width of the window</param>
		/// <param name="height">Height of the window</param>
		/// <param name="title">Title of the window</param>
		/// <param name="monitor">The monitor this window will be displayed on</param>
		/// <param name="share"></param>
		/// <returns>Returns a pointer to the window object</returns>
		[DllImport ( DLL, EntryPoint = "glfwCreateWindow" )]
		internal static extern IntPtr CreateWindow ( int width, int height, string title, IntPtr monitor, IntPtr share );

		/// <summary>
		/// Maximize window
		/// </summary>
		/// <param name="window">The window to maximize</param>
		[DllImport ( DLL, EntryPoint = "glfwMaximizeWindow" )]
		internal static extern void MaximizeWindow ( IntPtr window );

		/// <summary>
		/// Restore window
		/// </summary>
		/// <param name="window">The window to restore</param>
		[DllImport ( DLL, EntryPoint = "glfwRestoreWindow" )]
		internal static extern void RestoreWindow ( IntPtr window );

		/// <summary>
		/// Set the callback when the window is maximized/restore
		/// </summary>
		/// <param name="window">The window to detect maximize/restore</param>
		/// <param name="callback">The callback function</param>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowMaximizeCallback" )]
		internal static extern void SetWindowMaximizeCallback ( IntPtr window, Window.WinMaximizeCallback callback );

		/// <summary>
		/// Set the window's title
		/// </summary>
		/// <param name="window">The window to set the title</param>
		/// <param name="title">The title of the window</param>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowTitle" )]
		internal static extern void SetWindowTitle ( IntPtr window, string title );

		/// <summary>
		/// Set the window size
		/// </summary>
		/// <param name="window">The window to change the size of</param>
		/// <param name="width">The new window width</param>
		/// <param name="height">The new window height</param>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowSize" )]
		internal static extern void SetWindowSize ( IntPtr window, int width, int height );

		/// <summary>
		/// Get the window size
		/// </summary>
		/// <param name="window">The window to change the size of</param>
		/// <param name="width">The width of the window</param>
		/// <param name="height">The height of the window</param>
		[DllImport ( DLL, EntryPoint = "glfwGetWindowSize" )]
		internal static extern void GetWindowSize ( IntPtr window, out int width, out int height );

		/// <summary>
		/// Set the callback when the window is resized
		/// </summary>
		/// <param name="window">The window to detect resize</param>
		/// <param name="callback">The callback function</param>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowSizeCallback" )]
		internal static extern void SetWindowResizeCallback ( IntPtr window, Window.WinResizeCallback callback );

		/// <summary>
		/// Set the window's position
		/// </summary>
		/// <param name="window">The window to set the position</param>
		/// <param name="x">The x position on screen</param>
		/// <param name="y">The Y position on screen</param>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowPos" )]
		internal static extern void SetWindowPosition ( IntPtr window, int x, int y );

		/// <summary>
		/// Get the window's position
		/// </summary>
		/// <param name="window">The window to get the position</param>
		/// <param name="x">The x position on screen</param>
		/// <param name="y">The Y position on screen</param>
		[DllImport ( DLL, EntryPoint = "glfwGetWindowPos" )]
		internal static extern void GetWindowPosition ( IntPtr window, out int x, out int y );

		/// <summary>
		/// Set the window position change callback
		/// </summary>
		/// <param name="window">The window to set the position</param>
		/// <param name="callback">The callback function</param>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowPosCallback" )]
		internal static extern void SetWindowPositionCallback ( IntPtr window, Window.WinPosChangeCallback callback );

		/// <summary>
		/// Hide the whole window from the user (everything)
		/// </summary>
		/// <param name="window">The window to hide</param>
		[DllImport ( DLL, EntryPoint = "glfwHideWindow" )]
		internal static extern void HideWindow ( IntPtr window );

		/// <summary>
		/// Show the whole window from the user (everything)
		/// </summary>
		/// <param name="window">The window to show</param>
		[DllImport ( DLL, EntryPoint = "glfwShowWindow" )]
		internal static extern void ShowWindow ( IntPtr window );

		/// <summary>
		/// Force focus the window
		/// </summary>
		/// <param name="window">The window to focus</param>
		[DllImport ( DLL, EntryPoint = "glfwFocusWindow" )]
		internal static extern void FocusWindow ( IntPtr window );

		/// <summary>
		/// Request window attention from the user
		/// </summary>
		/// <param name="window">The window to request attention</param>
		[DllImport ( DLL, EntryPoint = "glfwRequestWindowAttention" )]
		internal static extern void RequestWindowAttention ( IntPtr window );

		/// <summary>
		/// Make the context for the GLFW window (allows hooks and events)
		/// </summary>
		/// <param name="window">The GLFW window to use</param>
		[DllImport ( DLL, EntryPoint = "glfwMakeContextCurrent" )]
		internal static extern void MakeContextCurrent ( IntPtr window );

		/// <summary>
		/// Set the close flag of the window
		/// </summary>
		/// <param name="window">The window to set flag</param>
		/// <param name="value">The value to set to</param>
		/// <returns>Return true if window should close</returns>
		[DllImport ( DLL, EntryPoint = "glfwSetWindowShouldClose" )]
		internal static extern void SetWindowShouldClose ( IntPtr window, int value );

		/// <summary>
		/// Returns the value of the close flag (whether the window should close or not)
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <returns>Return true if window should close</returns>
		[DllImport ( DLL, EntryPoint = "glfwWindowShouldClose" )]
		internal static extern bool WindowShouldClose ( IntPtr window );

		/// <summary>
		/// De story the window and all context (including all callbacks)
		/// </summary>
		/// <param name="window">The window to destroy</param>
		[DllImport ( DLL, EntryPoint = "glfwDestroyWindow" )]
		internal static extern void DestroyWindow ( IntPtr window );

		/// <summary>
		/// Clean up GLFW. Will require re-initialization before creating more windows
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glfwTerminate" )]
		internal static extern void Terminate ( );

		#endregion Window

		#region  Draw

		/// <summary>
		/// Swap between the back and front buffer to update the screen
		/// </summary>
		/// <param name="window">The GLFW window to swap</param>
		[DllImport ( DLL, EntryPoint = "glfwSwapBuffers" )]
		internal static extern void SwapBuffers ( IntPtr window );

		/// <summary>
		/// How many screen refreshes to wait before swamping the buffer (V-Sync)
		/// </summary>
		/// <param name="interval">The minimal number of screen refreshes before swamping</param>
		[DllImport ( DLL, EntryPoint = "glfwSwapInterval" )]
		internal static extern void SwapInterval ( int interval );

		#endregion Draw

		/// <summary>
		/// Set the error callback. This function will be called every time GLFW encounters an error
		/// </summary>
		/// <param name="callback"></param>
		[DllImport ( DLL, EntryPoint = "glfwSetErrorCallback" )]
		internal static extern void SetErrorCallback ( Game.ErrCallback callback );
	}
}
