﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Casper.DLLImport {
    internal static partial class OpenGL {
#if WINDOWS
		private const string DLL = "opengl32.dll";
#elif OSX
        private const string DLL = "/System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGL.dylib";
#elif LINUX
        private const string DLL = "GL";
#endif

		/// <summary>
		/// Get information string as a pointer
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[DllImport ( DLL, EntryPoint = "glGetString" )]
		private static extern IntPtr GLGetString ( Properties value );

		/// <summary>
		/// Get the information string
		/// </summary>
		/// <param name="value">The type of information requested</param>
		/// <returns>The information requested</returns>
		internal static String GetString ( Properties value ) => Marshal.PtrToStringAnsi ( GLGetString ( value ) );

		/// <summary>
		/// Get the integer value of the requested type
		/// </summary>
		/// <param name="type">The type of information</param>
		/// <param name="value">The int pointer to put the information in</param>
		[DllImport ( DLL, EntryPoint = "glGetIntegerv" )]
		internal static extern void GetIntegerv ( int type, out int value );

		/// <summary>
		/// Set the clear color
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glClearColor" )]
		internal static extern void ClearColor ( float read, float green, float blue, float alpha );

		/// <summary>
		/// Clear the screen
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glClear" )]
		internal static extern void Clear ( BitField mask );
	}
}
