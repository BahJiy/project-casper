using System;
using System.Runtime.InteropServices;

namespace Casper.DLLImport {
	internal static partial class GLFW {

		/// <summary>
		/// Process events (i.e. keyboard, resize, move, etc)
		/// </summary>
		[DllImport ( DLL, EntryPoint = "glfwPollEvents" )]
		internal static extern void GLFWPollEvents ( );

		/// <summary>
		/// Set input mode for either the cursor or key
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <param name="mode">Which input to set</param>
		/// <param name="value">The value to set. Set GLFW for more info on what is allowed</param>
		[DllImport ( DLL, EntryPoint = "glfwSetInputMode" )]
		internal static extern void SetInputMode ( IntPtr window, Inputs.InputType mode, int value );

		/// <summary>
		/// Get input mode for either the cursor or key
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <param name="mode">Which input to get</param>
		/// <returns>The enum number for the mode state</returns>
		[DllImport ( DLL, EntryPoint = "glfwGetInputMode" )]
		internal static extern int GetInputMode ( IntPtr window, Inputs.InputType mode );

		#region Mouse

		/// <summary>
		/// Get the last known status of the mouse button requested
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <param name="mouse">The mouse button to check</param>
		/// <returns></returns>
		[DllImport ( DLL, EntryPoint = "glfwGetMouseButton" )]
		internal static extern int GetMouseButtonState ( IntPtr window, Inputs.MouseButton mouse );

		/// <summary>
		/// Set the mouse position relative to the screen
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <param name="posX">The X position on screen</param>
		/// <param name="posY">The Y position on screen</param>
		[DllImport ( DLL, EntryPoint = "glfwSetCursorPos" )]
		internal static extern void SetCursorPos ( IntPtr window, double posX, double posY );

		/// <summary>
		/// Get the mouse position relative to the screen
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <param name="posX">The pointer to the X </param>
		/// <param name="posY">The pointer to the Y</param>
		[DllImport ( DLL, EntryPoint = "glfwGetCursorPos" )]
		internal static extern void GetCursorPos ( IntPtr window, out double posX, out double posY );

		#endregion Mouse

		#region Keyboard

		/// <summary>
		/// Set the keyboard callback. This function will be called every time a key's state changes
		/// </summary>
		/// <param name="window"></param>
		/// <param name="callback"></param>
		[DllImport ( DLL, EntryPoint = "glfwSetKeyCallback" )]
		internal static extern void SetKeyboardCallback ( IntPtr window, Inputs.Keyboard.KBCallback callback );

		/// <summary>
		/// Get the last known status of the key requested
		/// </summary>
		/// <param name="window">The window to check</param>
		/// <param name="key">The key to check</param>
		/// <returns></returns>
		[DllImport ( DLL, EntryPoint = "glfwGetKey" )]
		internal static extern int GetKey ( IntPtr window, Inputs.KeyCode key );

		#endregion Keyboard
	}
}
