using System;
namespace Casper.DLLImport {
	public enum GLFWEnum {
		/*! @brief Input focus window hint and attribute
		 *
		 *  Input focus [window hint](@ref GLFW_FOCUSED_hint) or
		 *  [window attribute](@ref GLFW_FOCUSED_attrib).
		 */
		FOCUSED = 0x00020001,
		/*! @brief Window iconification window attribute
		 *
		 *  Window iconification [window attribute](@ref GLFW_ICONIFIED_attrib).
		 */
		ICONIFIED = 0x00020002,
		/*! @brief Window resize-ability window hint and attribute
		 *
		 *  Window resize-ability [window hint](@ref GLFW_RESIZABLE_hint) and
		 *  [window attribute](@ref GLFW_RESIZABLE_attrib).
		 */
		RESIZABLE = 0x00020003,
		/*! @brief Window visibility window hint and attribute
		 *
		 *  Window visibility [window hint](@ref GLFW_VISIBLE_hint) and
		 *  [window attribute](@ref GLFW_VISIBLE_attrib).
		 */
		VISIBLE = 0x00020004,
		/*! @brief Window decoration window hint and attribute
		 *
		 *  Window decoration [window hint](@ref GLFW_DECORATED_hint) and
		 *  [window attribute](@ref GLFW_DECORATED_attrib).
		 */
		DECORATED = 0x00020005,
		/*! @brief Window auto-iconification window hint and attribute
		 *
		 *  Window auto-iconification [window hint](@ref GLFW_AUTO_ICONIFY_hint) and
		 *  [window attribute](@ref GLFW_AUTO_ICONIFY_attrib).
		 */
		AUTO_ICONIFY = 0x00020006,
		/*! @brief Window decoration window hint and attribute
		 *
		 *  Window decoration [window hint](@ref GLFW_FLOATING_hint) and
		 *  [window attribute](@ref GLFW_FLOATING_attrib).
		 */
		FLOATING = 0x00020007,
		/*! @brief Window maximization window hint and attribute
		 *
		 *  Window maximization [window hint](@ref GLFW_MAXIMIZED_hint) and
		 *  [window attribute](@ref GLFW_MAXIMIZED_attrib).
		 */
		MAXIMIZED = 0x00020008,
		/*! @brief Cursor centering window hint
		 *
		 *  Cursor centering [window hint](@ref GLFW_CENTER_CURSOR_hint).
		 */
		CENTER_CURSOR = 0x00020009,
		/*! @brief Window framebuffer transparency hint and attribute
		 *
		 *  Window framebuffer transparency
		 *  [window hint](@ref GLFW_TRANSPARENT_FRAMEBUFFER_hint) and
		 *  [window attribute](@ref GLFW_TRANSPARENT_FRAMEBUFFER_attrib).
		 */
		TRANSPARENT_FRAMEBUFFER = 0x0002000A,
		/*! @brief Mouse cursor hover window attribute.
		 *
		 *  Mouse cursor hover [window attribute](@ref GLFW_HOVERED_attrib).
		 */
		HOVERED = 0x0002000B,
		/*! @brief Input focus on calling show window hint and attribute
		 *
		 *  Input focus [window hint](@ref GLFW_FOCUS_ON_SHOW_hint) or
		 *  [window attribute](@ref GLFW_FOCUS_ON_SHOW_attrib).
		 */
		FOCUS_ON_SHOW = 0x0002000C,

		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_RED_BITS).
		 */
		RED_BITS = 0x00021001,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_GREEN_BITS).
		 */
		GREEN_BITS = 0x00021002,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_BLUE_BITS).
		 */
		BLUE_BITS = 0x00021003,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_ALPHA_BITS).
		 */
		ALPHA_BITS = 0x00021004,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_DEPTH_BITS).
		 */
		DEPTH_BITS = 0x00021005,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_STENCIL_BITS).
		 */
		STENCIL_BITS = 0x00021006,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_ACCUM_RED_BITS).
		 */
		ACCUM_RED_BITS = 0x00021007,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_ACCUM_GREEN_BITS).
		 */
		ACCUM_GREEN_BITS = 0x00021008,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_ACCUM_BLUE_BITS).
		 */
		ACCUM_BLUE_BITS = 0x00021009,
		/*! @brief Framebuffer bit depth hint.
		 *
		 *  Framebuffer bit depth [hint](@ref GLFW_ACCUM_ALPHA_BITS).
		 */
		ACCUM_ALPHA_BITS = 0x0002100A,
		/*! @brief Framebuffer auxiliary buffer hint.
		 *
		 *  Framebuffer auxiliary buffer [hint](@ref GLFW_AUX_BUFFERS).
		 */
		AUX_BUFFERS = 0x0002100B,
		/*! @brief OpenGL stereoscopic rendering hint.
		 *
		 *  OpenGL stereoscopic rendering [hint](@ref GLFW_STEREO).
		 */
		STEREO = 0x0002100C,
		/*! @brief Framebuffer MSAA samples hint.
		 *
		 *  Framebuffer MSAA samples [hint](@ref GLFW_SAMPLES).
		 */
		SAMPLES = 0x0002100D,
		/*! @brief Framebuffer sRGB hint.
		 *
		 *  Framebuffer sRGB [hint](@ref GLFW_SRGB_CAPABLE).
		 */
		SRGB_CAPABLE = 0x0002100E,
		/*! @brief Monitor refresh rate hint.
		 *
		 *  Monitor refresh rate [hint](@ref GLFW_REFRESH_RATE).
		 */
		REFRESH_RATE = 0x0002100F,
		/*! @brief Framebuffer double buffering hint.
		 *
		 *  Framebuffer double buffering [hint](@ref GLFW_DOUBLEBUFFER).
		 */
		DOUBLEBUFFER = 0x00021010,

		/*! @brief Context client API hint and attribute.
		 *
		 *  Context client API [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CLIENT_API = 0x00022001,
		/*! @brief Context client API major version hint and attribute.
		 *
		 *  Context client API major version [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CONTEXT_VERSION_MAJOR = 0x00022002,
		/*! @brief Context client API minor version hint and attribute.
		 *
		 *  Context client API minor version [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CONTEXT_VERSION_MINOR = 0x00022003,
		/*! @brief Context client API revision number hint and attribute.
		 *
		 *  Context client API revision number [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CONTEXT_REVISION = 0x00022004,
		/*! @brief Context robustness hint and attribute.
		 *
		 *  Context client API revision number [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CONTEXT_ROBUSTNESS = 0x00022005,
		/*! @brief OpenGL forward-compatibility hint and attribute.
		 *
		 *  OpenGL forward-compatibility [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		OPENGL_FORWARD_COMPAT = 0x00022006,
		/*! @brief OpenGL debug context hint and attribute.
		 *
		 *  OpenGL debug context [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		OPENGL_DEBUG_CONTEXT = 0x00022007,
		/*! @brief OpenGL profile hint and attribute.
		 *
		 *  OpenGL profile [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		OPENGL_PROFILE = 0x00022008,
		/*! @brief Context flush-on-release hint and attribute.
		 *
		 *  Context flush-on-release [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CONTEXT_RELEASE_BEHAVIOR = 0x00022009,
		/*! @brief Context error suppression hint and attribute.
		 *
		 *  Context error suppression [hint](@ref GLFW_CLIENT_API_hint) and
		 *  [attribute](@ref GLFW_CLIENT_API_attrib).
		 */
		CONTEXT_NO_ERROR = 0x0002200A

	}
}
