﻿using System;
using System.Collections.Generic;
using System.Text;
using Casper.Graphic;
using Casper.DLLImport;

namespace Casper.Inputs {
	/// <summary>
	/// This class handles all keyboard events and state
	/// </summary>
    public sealed class Keyboard {
		/// <summary>
		/// A method stub for GLFW keyboard callback
		/// </summary>
		internal delegate void KBCallback ( IntPtr window, KeyCode key, int scanCode, ButtonState state, KeyMods mods );
		/// <summary>
		/// Occurs when the state of the keyboard changes because a key has been press or released
		/// </summary>
		public event EventHandler <KeyboardEventArgs> KeyboardCallbackEvent;
		/// <summary>
		/// The method that GLFW will call when a key is press/release
		/// </summary>
		/// <param name="window">The window that the key was detected on</param>
		/// <param name="key">The key code</param>
		/// <param name="scanCode">The scan code</param>
		/// <param name="state">Whether the key was press/release</param>
		/// <param name="mods">Any modifier like shift or ctrl</param>
		private void KeyboardCallbackFunction ( IntPtr window, KeyCode key, int scanCode, ButtonState state, KeyMods mods ) =>
			KeyboardCallbackEvent?.Invoke ( this, new KeyboardEventArgs ( ) {
				key = key,
				scanCode = scanCode,
				state = state,
				modifier = mods
			} );

		private readonly Window window;

		/// <summary>
		/// Initialize a new instance of Keyboard
		/// </summary>
		/// <param name="window">The window that this keyboard should hook up to</param>
		internal Keyboard ( Window window ) {
			this.window = window;
			GLFW.SetKeyboardCallback ( window.window, KeyboardCallbackFunction );
		}

		/// <summary>
		/// Get the last known state of the requested key
		/// </summary>
		/// <param name="key">The key to check</param>
		/// <returns>The last known button state of the key</returns>
		public ButtonState GetKeyState ( KeyCode key )
			=> ( ButtonState )GLFW.GetKey ( window, key );
	}

	/// <summary>
	/// Provides data for the Keyboard.KeyboardCallbackEvent. This class cannot be inherited.
	/// </summary>
	public sealed class KeyboardEventArgs : EventArgs {
		public KeyCode key { get; internal set; }
		public int scanCode { get; internal set; }
		public ButtonState state { get; internal set; }
		public KeyMods modifier { get; internal set; }
	}
} 