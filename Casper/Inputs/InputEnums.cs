﻿using Casper.DLLImport;

namespace Casper.Inputs {
	/// <summary>
	/// A list of all key modifier (i.e. shift, ctrl)
	/// </summary>
	public enum KeyMods {
		SHIFT = 0x0001,
		CONTROL = 0x0002,
		ALT = 0x0004,
		SUPER = 0x0008,
	}

	/// <summary>
	/// A list of all keyboard keys
	/// </summary>
	public enum KeyCode {
		SPACE = 32,
		APOSTROPHE = 39, /* ' */
		COMMA = 44, /* , */
		MINUS = 45, /* - */
		PERIOD = 46, /* . */
		SLASH = 47, /* / */
		ZERO = 48,
		ONE = 49,
		TWO = 50,
		THREE = 51,
		FOUR = 52,
		FIVE = 53,
		SIX = 54,
		SEVEN = 55,
		EIGHT = 56,
		NINE = 57,
		SEMICOLON = 59, /* ; */
		EQUAL = 61, /* = */
		A = 65,
		B = 66,
		C = 67,
		D = 68,
		E = 69,
		F = 70,
		G = 71,
		H = 72,
		I = 73,
		J = 74,
		K = 75,
		L = 76,
		M = 77,
		N = 78,
		O = 79,
		P = 80,
		Q = 81,
		R = 82,
		S = 83,
		T = 84,
		U = 85,
		V = 86,
		W = 87,
		X = 88,
		Y = 89,
		Z = 90,
		LEFT_BRACKET = 91, /* [ */
		BACKSLASH = 92, /* \ */
		RIGHT_BRACKET = 93, /* ] */
		TILDA = 96, /* ` */
		WORLD_1 = 161, /* non-US #1 */
		WORLD_2 = 162, /* non-US #2 */

		/* Function keys */
		ESCAPE = 256,
		ENTER = 257,
		TAB = 258,
		BACKSPACE = 259,
		INSERT = 260,
		DELETE = 261,
		RIGHT = 262,
		LEFT = 263,
		DOWN = 264,
		UP = 265,
		PAGE_UP = 266,
		PAGE_DOWN = 267,
		HOME = 268,
		END = 269,
		CAPS_LOCK = 280,
		SCROLL_LOCK = 281,
		NUM_LOCK = 282,
		PRINT_SCREEN = 283,
		PAUSE = 284,
		F1 = 290,
		F2 = 291,
		F3 = 292,
		F4 = 293,
		F5 = 294,
		F6 = 295,
		F7 = 296,
		F8 = 297,
		F9 = 298,
		F10 = 299,
		F11 = 300,
		F12 = 301,
		F13 = 302,
		F14 = 303,
		F15 = 304,
		F16 = 305,
		F17 = 306,
		F18 = 307,
		F19 = 308,
		F20 = 309,
		F21 = 310,
		F22 = 311,
		F23 = 312,
		F24 = 313,
		F25 = 314,
		KP_0 = 320,
		NUMPAD_1 = 321,
		NUMPAD_2 = 322,
		NUMPAD_3 = 323,
		NUMPAD_4 = 324,
		NUMPAD_5 = 325,
		NUMPAD_6 = 326,
		NUMPAD_7 = 327,
		NUMPAD_8 = 328,
		NUMPAD_9 = 329,
		NUMPAD_DECIMAL = 330,
		NUMPAD_DIVIDE = 331,
		NUMPAD_MULTIPLY = 332,
		NUMPAD_SUBTRACT = 333,
		NUMPAD_ADD = 334,
		NUMPAD_ENTER = 335,
		NUMPAD_EQUAL = 336,
		LEFT_SHIFT = 340,
		LEFT_CONTROL = 341,
		LEFT_ALT = 342,
		LEFT_SUPER = 343,
		RIGHT_SHIFT = 344,
		RIGHT_CONTROL = 345,
		RIGHT_ALT = 346,
		RIGHT_SUPER = 347,
		MENU = 348,
	}

	/// <summary>
	/// Contains all possible button states
	/// </summary>
	public enum ButtonState {
		RELEASE = 0,
		PRESS = 1,
		REPEAT = 2
	}

	/// <summary>
	/// Enum for Mouse Buttons
	/// </summary>
	public enum MouseButton {
		LEFT = 0,
		RIGHT = 1,
		MIDDLE = 2,
		BUTTON_1 = LEFT,
		BUTTON_2 = RIGHT,
		BUTTON_3 = MIDDLE,
		BUTTON_4 = 3,
		BUTTON_5 = 4,
		BUTTON_6 = 5,
		BUTTON_7 = 6,
		BUTTON_8 = 7
	}

	/// <summary>
	/// Enum for Cursor Shape (i.e. arrow, I-beam, etc)
	/// </summary>
	public enum CursorShape {
		/// <summary> The regular arrow cursor </summary>
		ARROW = 0x00036001,
		/// <summary> The text input I-beam cursor shape </summary>
		IBEAM = 0x00036002,
		/// <summary> The crosshair shape </summary>
		CROSSHAIR = 0x00036003,
		/// <summary> The hand shape </summary>
		HAND = 0x00036004,
		/// <summary> The horizontal resize arrow shape </summary>
		HRESIZE = 0x00036005,
		/// <summary> The vertical resize arrow shape </summary>
		VRESIZE = 0x00036006
	}
	/// <summary>

	/// <summary>
	/// Contains all the allowed cursor mode
	/// </summary>
	public enum CursorMode {
		/// <summary> Set the cursor to default behavior </summary>
		NORMAL = 0x00034001,
		/// <summary> Hides the cursor </summary>
		HIDDEN = 0x00034002,
		/// <summary> Hides the cursor and grabs the cursor </summary>
		DISABLED = 0x00034003,
	}

	/// <summary>
	/// Type of inputs
	/// </summary>
	internal enum InputType {
		CURSOR = 0x00033001,
		STICKY_KEYS = 0x00033002,
		STICKY_MOUSE_BUTTONS = 0x00033003,
	}

	public static class Extensions {
		public static bool IsPress ( this ButtonState state ) => state == ButtonState.PRESS;
		public static bool IsRelease ( this ButtonState state ) => state == ButtonState.RELEASE;
		public static bool IsRepeat ( this ButtonState state ) => state == ButtonState.REPEAT;
	}
}