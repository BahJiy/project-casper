﻿using System;
using System.Collections.Generic;
using System.Text;
using Casper.DLLImport;
using Casper.Graphic;

namespace Casper.Inputs {
	/// <summary>
	/// This class handles all mouse events and state
	/// </summary>
	public sealed class Mouse {
		private readonly Window window;

		internal Mouse ( Window window ) {
			this.window = window;
		}

		/// <summary>
		/// Get the last known state of the mouse's button
		/// </summary>
		/// <param name="button">The mouse button to check</param>
		/// <returns>The last known state of the mouse's button</returns>
		public ButtonState GetMouseState ( MouseButton button )
			=> ( ButtonState ) GLFW.GetMouseButtonState ( window, button );

		/// <summary>
		/// Set the mouse position on screen
		/// </summary>
		/// <param name="x">X coordinate</param>
		/// <param name="y">Y coordinate</param>
		public void SetMousePosition ( double x, double y )
			=> GLFW.SetCursorPos ( window, x, y );

		/// <summary>
		/// Set the mouse position on screen
		/// </summary>
		/// <param name="position">The Vector2 containing the new mouse position</param>
		public void SetMousePosition ( Vector2 position )
			=> GLFW.SetCursorPos ( window, position.X, position.Y );

		/// <summary>
		/// Get the mouse position on screen
		/// </summary>
		/// <returns>The vector containing the mouse position on screen</returns>
		public Vector2 GetMousePsition ( ) {
			GLFW.GetCursorPos ( window, out double x, out double y );
			return new Vector2 ( x, y );
		}

		public void SetMode ( CursorMode mode )
			=> GLFW.SetInputMode ( window, InputType.CURSOR, ( int ) mode );

		public CursorMode GetMode ( )
			=> ( CursorMode ) GLFW.GetInputMode ( window, InputType.CURSOR );
	}
}
