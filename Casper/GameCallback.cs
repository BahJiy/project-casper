using System;

namespace Casper {
	public partial class Game {

		/// <summary>
		/// Occurs when the game receives an error
		/// </summary>
		public event EventHandler<ErrorEventArgs> ErrorCallbackEvent;
		/// <summary>
		/// A method stub for GLFW error callback
		/// </summary>
		internal delegate void ErrCallback ( ErrorCode errorCode, string description );
		private void ErrorCallbackHander ( ErrorCode code, String description ) =>
			ErrorCallbackEvent?.Invoke ( this, new ErrorEventArgs ( ) { errorCode = code, description = description } );
	}

	/// <summary>
	/// Provides data for the Game.ErrorCallbackEvent. This class cannot be inherited.
	/// </summary>
	public sealed class ErrorEventArgs : EventArgs {
		public ErrorCode errorCode { get; set; }
		public string description { get; set; }
	}

}
