using System;

namespace Casper.Sprite {
	public sealed class Rectangle {
		public Vector2 Position { get; set; }
		public Vector2 Size { get; set; }

		public Rectangle ( Vector2 position, Vector2 size ) {
			this.Position = position;
			this.Size = size;
		}
		public Rectangle ( double x, double y, double width, double height ) : this ( new Vector2 ( x, y ), new Vector2 ( width, height ) ) { }

		public override string ToString ( ) => $"Position: {Position.ToString ( )} | Size: {Size.X}x{Size.Y}";
		public override bool Equals ( object obj ) => obj is Rectangle rect ? Position.Equals ( rect.Position ) && Size.Equals ( rect.Size ) : false;
		public override int GetHashCode ( ) => Position.GetHashCode ( ) * 17 + Size.GetHashCode ( ) * 7;
	}
}
