using System;

namespace Casper.Sprite {

	public interface Sprite {
		Vector2 Velocity { get; set; }
		Vector2 Position { get; set; }
		double X { get { return Position.X; } }
		double Y { get { return Position.Y; } }
		double Magnitude ( );
	}
}
